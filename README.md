# rehype-obsidian-callout

Rehype plugin to render [Obsidian callouts](https://help.obsidian.md/Editing+and+formatting/Callouts).

# What’s this?

This package is a [unified](https://github.com/unifiedjs/unified) ([rehype](https://github.com/rehypejs/rehype)) plugin to add support for Obsidian callouts.

## Installation

This package is [ESM only](https://gist.github.com/sindresorhus/a39789f98801d908bbc7ff3ecc99d99c).

```bash
npm install rehype-obsidian-callout
```

In Deno, with esm.sh:

```js
import rehypeObsidian from "https://esm.sh/rehype-obsidian-callout@1.0.1";
```

In browsers, with esm.sh:

```html
<script type="module">
    import rehypeObsidianCallout from 'https://esm.sh/rehype-obsidian-callout?bundle';
</script>
```

## Use

The markdown file with a callout:

```
> [!note]- I am a *collapsible* callout
> Some note shown after opening
```

And a script:

```js
import { unified } from "unified";
import remarkParse from "remark-parse";
import remarkRehype from "remark-rehype";
import rehypeStringify from "rehype-stringify";
import plugin from "rehype-obsidian-callout";

const input = `
> [!note]- I am a *collapsible* callout
> Some note shown after opening
`;

const result = await unified()
  .use(remarkParse)
  .use(remarkRehype)
  .use(plugin)
  .use(rehypeStringify)
  .process(input);

console.log(String(result));
```

Running the script yields (formatted for readability):

```html
<details class="callout-type-note callout-block callout-collapsible">
  <summary class="callout-title-section">
    <span class="callout-icon-wrapper">
      <!-- svg icon -->
    </span>
    <p class="callout-title"> I am a <em>collapsible</em> callout</p>
  </summary>
  <div class="callout-content-section">
    <p>Some note shown after opening</p>
  </div>
</details>
```

With default styles this renders as:

![callout visuals](https://gitlab.com/staticnoise/rehype-obsidian-callout/-/raw/main/screenshot.png)

You can see an interactive example here: [codepen.io/staticnoise/pen/wvEVjOR](https://codepen.io/staticnoise/pen/wvEVjOR).


### Use with react-markdown

You can see an interactive example here: [codepen.io/staticnoise/pen/rNqBwzv](https://codepen.io/staticnoise/pen/rNqBwzv).

```jsx
<ReactMarkdown 
  rehypePlugins={[
    rehypeObsidian
  ]}>
    {input}
</ReactMarkdown>
```

## Configuration

```ts
export interface Config {
  callouts: Record<string, string>;
  showIcon: boolean;
  iconTagName: string;
}
```

## Features

- supports markdown syntax in titles
- modifies HTML instead of Markdown (no need for `allowDangerousHtml` in [remark-rehype](https://github.com/remarkjs/remark-rehype#optionsallowdangeroushtml) plugin)
- supports custom callouts (icons as well as CSS)
- no JavaScript is required for handling collapsible callouts (the implementation uses a `details` tag)

## Similar packages

- [remark-obsidian-callout](https://www.npmjs.com/package/remark-obsidian-callout)
- [@microflash/remark-callout-directives](https://www.npmjs.com/package/@microflash/remark-callout-directives)
- [@flowershow/remark-callouts](https://www.npmjs.com/package/@flowershow/remark-callouts)

## Credits

- [remark-obsidian-callout](https://www.npmjs.com/package/remark-obsidian-callout) for default icons and configuration file intuition.

## Licence

The default icons come from [Lucide](https://lucide.dev/license) and are licensed under the ISC License.

The source code is licensed under the MIT licence (see below).

```
Copyright (c) 2023 Adam Chovanec

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
```
